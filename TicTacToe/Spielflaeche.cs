﻿using System.Windows.Controls;

public class Spielflaeche : Button
{
    public SpielflaechenDaten data = new SpielflaechenDaten();

    public Spielflaeche()
    {
        Height = 72;
        Width = 72;
        FontSize = 40;
        HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        VerticalAlignment = System.Windows.VerticalAlignment.Center;
    }

    


    public void AddToSurface(Canvas can, int posX, int posY)
    {
        Canvas.SetLeft(this, ((posX + 1) * 72 + posX * 72) - 10);
        Canvas.SetTop(this, ((posY + 1) * 72 + posY * 72));
        can.Children.Add(this);
    }
}