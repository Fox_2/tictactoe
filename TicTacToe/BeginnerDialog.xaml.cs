﻿using System.Windows;
using System.Windows.Controls;

namespace TicTacToe
{
    public partial class BeginnerDialog : Window
    {
        public BeginnerDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow(((Button)sender).Content.ToString()).Show();
            Close();
        }
    }
}
