﻿
public class Row
{
    public Spielflaeche[] rowData = new Spielflaeche[3];

    public Row(Spielflaeche f1, Spielflaeche f2, Spielflaeche f3)
    {
        rowData[0] = f1;
        rowData[1] = f2;
        rowData[2] = f3;
    }

    public State RowWon()
    {
        if(rowData[0].data.Status == rowData[1].data.Status 
            && rowData[1].data.Status == rowData[2].data.Status 
            && rowData[0].data.Status != State.None)
        {
            return rowData[0].data.Status;
        }
        return State.None;
    }

    public SpielflaechenDaten GetRowData(State toCheck)
    {
        byte count = 0;
        int x = 5, y = 5;
        foreach(Spielflaeche rd in rowData)
        {
            if(rd.data.Status == toCheck)
            {
                ++count;
            }
            else
            {
                x = rd.data.X;
                y = rd.data.Y;
            }
        }

        if (count == 2)
        {
            return new SpielflaechenDaten { X = x, Y = y, Status = toCheck };
        }
        else
        {
            return null;
        }
    }
}

