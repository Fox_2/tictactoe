﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TicTacToe
{
    public partial class MainWindow : Window
    {
        public Spielflaeche[,] spielbutton = new Spielflaeche[3, 3];
        public Row[] rows = new Row[8];
        private bool kiFirstTurn = false;
        private bool roundOne = true;
        private bool roundTwo = false;
        private State playerState;
        private byte drawcounter = 0;

        public State PlayerState
        {
            get
            {
                return playerState;
            }
            set
            {
                playerState = value;
                SetKIState();
            }
        }

        public Beginner beginner = Beginner.Zufall;
        public State computerState = State.O;
        public Random zgen = new Random();

        private bool playInProgress = false;
        public bool PlayInProgress
        {
            get
            {
                return playInProgress;
            }
            set
            {
                playInProgress = value;
                MenuControlDuringGame(playAs);
            }
        }

        public MainWindow(string first)
        {
            PlayerState = (State)zgen.Next(2);
            SetBeginner(first);
            InitializeComponent();
            SetBeginnerChecked(first);
            CreatePlayField();
            Beginn();
        }

        private void SetBeginnerChecked(string first)
        {
            foreach(object o in FirstTurn.Items)
            {
                if(o is MenuItem && 
                    ((MenuItem)o).IsCheckable && 
                    ((MenuItem)o).Header.ToString().Equals(first, StringComparison.CurrentCultureIgnoreCase))
                {
                    ((MenuItem)o).IsChecked = true;
                    RadioClick(o);
                }
            }
        }

        private void Beginn()
        {
            switch (beginner)
            {
                case Beginner.Computer:
                    roundOne = false;
                    kiFirstTurn = true;
                    KI();
                    break;

                case Beginner.Zufall:
                    if (zgen.Next(2)==0)
                    {
                        roundOne = false;
                        kiFirstTurn = true;
                        KI();
                    }
                    break;
            }
        }

        private void SetPlayerState(string input)
        {
            switch (input)
            {
                case "X":
                    PlayerState = State.X;
                    break;
                case "O":
                    PlayerState = State.O;
                    break;
                case "Zufall":
                    PlayerState = (State)zgen.Next(2);
                    break;
            }
        }

        private void SetKIState()
        {
            switch (PlayerState)
            {
                case State.X:
                    computerState = State.O;
                    break;
                case State.O:
                    computerState = State.X;
                    break;
            }
        }

        private void SetBeginner(string v)
        {
            switch (v)
            {
                case "Computer":
                    beginner = Beginner.Computer;
                    break;
                case "Spieler":
                    beginner = Beginner.Player;
                    break;
                case "Zufall":
                    beginner = Beginner.Zufall;
                    break;
            }
        }


        private void CreatePlayField()
        {
            CreateButtons();
            FillRows();
        }

        private void CreateButtons()
        {
            for (byte b1 = 0; b1 < spielbutton.GetLength(0); ++b1)
            {
                for (byte b2 = 0; b2 < spielbutton.GetLength(0); ++b2)
                {
                    spielbutton[b1, b2] = new Spielflaeche();
                    spielbutton[b1, b2].data.X = b1;
                    spielbutton[b1, b2].data.Y = b2;
                    spielbutton[b1, b2].data.Status = State.None;
                    spielbutton[b1, b2].AddToSurface(can, b1, b2);
                    spielbutton[b1, b2].Click += ButtonClick;
                }
            }
        }

        public void FillRows()
        {
            byte rowcounter = 0;
            for (byte b1 = 0; b1 < spielbutton.GetLength(0); ++b1)
            {
                rows[rowcounter] = new Row(spielbutton[b1, 0], spielbutton[b1, 1], spielbutton[b1, 2]);
                ++rowcounter;
                rows[rowcounter] = new Row(spielbutton[0, b1], spielbutton[1, b1], spielbutton[2, b1]);
                ++rowcounter;
            }
            rows[rowcounter] = new Row(spielbutton[0, 0], spielbutton[1, 1], spielbutton[2, 2]);
            rows[rowcounter + 1] = new Row(spielbutton[0, 2], spielbutton[1, 1], spielbutton[2, 0]);
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (((Spielflaeche)sender).data.Status != State.None)
            {
                return;
            }
            if(Change(((Spielflaeche)sender).data.X, ((Spielflaeche)sender).data.Y, PlayerState))
            {
                KI();
            }
        }

        private bool Change(int x, int y, State state)
        {
            spielbutton[x, y].data.Status = state;
            spielbutton[x, y].Content = state.ToString();
            if (!kiFirstTurn)
            {
                PlayInProgress = true;
            }
            ++drawcounter;
            kiFirstTurn = false;
            return !Sieg();
        }

        public bool Sieg()
        {
            foreach (Row r in rows)
            {
                State st = r.RowWon();
                if (st == State.X || st == State.O)
                {
                    DisplayWin(st);
                    return true;
                }
            }
            if (drawcounter == 9)
            {
                DisplayWin(State.None);
                return true;
            }

            return false;
        }

        public void DisplayWin(State st)
        {
            if (st == PlayerState)
            {
                MessageBox.Show("Player (" + st.ToString() + ") won.\nCongratulation!");
            }
            else if (st == computerState)
            {
                MessageBox.Show("Computer (" + st.ToString() + ") won.\nBetter luck next time!");
            }
            else if (st == State.None)
            {
                MessageBox.Show("Draw!");
            }
            Restart();
        }

        public void Restart()
        {
            System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Restart?", "Game ended", System.Windows.Forms.MessageBoxButtons.YesNo);
            if(result== System.Windows.Forms.DialogResult.Yes)
            {
                Reset();
            }
            else if(result == System.Windows.Forms.DialogResult.No)
            {
                Environment.Exit(0);
            }
        }

        private void Beginn_Click(object sender, RoutedEventArgs e)
        {
            RadioClick(sender);
            SetBeginner(((MenuItem)sender).Header.ToString());
        }

        

        public void MenuControlDuringGame(MenuItem item)
        {
            foreach (object o in item.Items)
            {
                if (o is MenuItem && ((MenuItem)o).IsCheckable)
                {
                    ((MenuItem)o).IsEnabled = !PlayInProgress;
                }
            }
        }

        private void PlayerState_Click(object sender, RoutedEventArgs e)
        {
            RadioClick(sender);
            SetPlayerState(((MenuItem)sender).Header.ToString());
            if (((MenuItem)sender).Header.ToString().Equals("Zufall", StringComparison.CurrentCultureIgnoreCase)
                && zgen.Next(2) == 0)
            {
                return;
            }
            Swap();
        }

        private void Swap()
        {
            foreach(Spielflaeche sf in spielbutton)
            {
                if (sf.data.Status == State.O)
                {
                    sf.data.Status = State.X;
                    sf.Content = sf.data.Status.ToString();
                }
                else if(sf.data.Status == State.X)
                {
                    sf.data.Status = State.O;
                    sf.Content = sf.data.Status.ToString();
                }
            }
        }

        public void RadioClick(object sender)
        {
            MenuItem item = (MenuItem)sender;
            MenuItem menu = (MenuItem)item.Parent;
            foreach (object o in menu.Items)
            {
                if(o is MenuItem)
                { 
                    ((MenuItem)o).IsChecked = false;
                }
            }
            item.IsChecked = true;
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            drawcounter = 0;
            foreach (Spielflaeche sf in spielbutton)
            {
                sf.Content = "";
                sf.data.Status = State.None;
            }
            PlayInProgress = false;

            if (RandomSign.IsChecked == true)
            {
                PlayerState = (State)zgen.Next(2);
            }
            roundOne = true;
            roundTwo = false;
            Beginn();
            
        }

        //////////////////////////////////////////////////////////////////////////////////////////////KI
        
        public void KI()
        {
            if (zgen.Next(21) == 0)
            {
                KI_Random();
            }
            else
            {
                CheckRows();
            }
            roundOne = false;
        }

        private void CheckRows()
        {
            if (CheckWinOrLossKI(computerState))
            {
                return;
            }

            if (CheckWinOrLossKI(PlayerState))
            {
                return;
            }

            if (CheckEdges())
            {
                return;
            }

            KI_Random();
            
        }

        

        public bool CheckWinOrLossKI(State st)
        {
            foreach(Row r in rows)
            {
                SpielflaechenDaten data = r.GetRowData(st);
                if(data != null && spielbutton[data.X, data.Y].data.Status == State.None)
                {
                    Change(data.X, data.Y, computerState);
                    return true;
                }
            }
            return false;
        }


        private bool CheckEdges()
        {
            if (roundOne &&
                (spielbutton[0, 0].data.Status == PlayerState ||
                 spielbutton[2, 0].data.Status == PlayerState ||
                 spielbutton[0, 2].data.Status == PlayerState ||
                 spielbutton[2, 2].data.Status == PlayerState))
            {
                Change(1, 1, computerState);
                roundTwo = true;
                return true;
            }

            if (roundTwo)
            {
                SetRandomBorder();
                roundTwo = false;
                return true;
            }
            return false;
        }

        private void SetRandomBorder()
        {
            while (true)
            {
                switch (zgen.Next(4))
                {
                    case 0:
                        if (spielbutton[0, 1].data.Status != State.None) { continue; }
                        Change(0, 1, computerState);
                        return;
                    case 1:
                        if (spielbutton[1, 0].data.Status != State.None) { continue; }
                        Change(1, 0, computerState);
                        return;
                    case 2:
                        if (spielbutton[1, 2].data.Status != State.None) { continue; }
                        Change(1, 2, computerState);
                        return;
                    case 3:
                        if (spielbutton[2, 1].data.Status != State.None) { continue; }
                        Change(2, 1, computerState);
                        return;
                }
            }
        }


        private void KI_Random()
        {
            while (true)
            {
                int x = zgen.Next(3);
                int y = zgen.Next(3);
                if (spielbutton[x, y].data.Status == State.None)
                {
                    Change(x, y, computerState);
                    break;
                }
            }
        }
    }
}

